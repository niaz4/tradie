package com.example.tradie.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TradePeopleSetReqDto {

    private String username;
    private String email;
}
