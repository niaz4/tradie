package com.example.tradie.repositories;

import com.example.tradie.models.TradesPeople;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface TradesPeopleRepository extends ReactiveMongoRepository<TradesPeople, String> {
    Mono<TradesPeople> findByUsernameAndEmail(String username, String email);
}
