package com.example.tradie.repositories;

import com.example.tradie.models.Job;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface JobsRepository extends ReactiveMongoRepository<Job, String> {

    Mono<Job> findByProjectId(String projectId);
    Mono<Job> findByTradesPersonId(String tradesPersonId);
    Mono<Job> findByBidId(String bidId);
}
