package com.example.tradie.repositories;

import com.example.tradie.models.Project;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface ProjectRepository extends ReactiveMongoRepository<Project, String> {

    Flux<Project> findAllByOrderByCreatedAtDesc();
}
