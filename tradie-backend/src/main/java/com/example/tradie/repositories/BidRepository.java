package com.example.tradie.repositories;

import com.example.tradie.models.Bid;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface BidRepository extends ReactiveMongoRepository<Bid, String> {

    Flux<Bid> findAllByProjectIdOrderByCreatedAtDesc(String projectId);
    Flux<Bid> findAllByTradesPeopleId(String tradesPersonId);
}
