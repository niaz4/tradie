package com.example.tradie.services;

import com.example.tradie.models.Bid;
import com.example.tradie.repositories.BidRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class BidService {

    private final BidRepository bidRepository;

    public Mono<Bid> save(Bid bid){
        return bidRepository.save(bid);
    }

    public Flux<Bid> findAllByProjectId(String projectId){
        return bidRepository.findAllByProjectIdOrderByCreatedAtDesc(projectId);
    }

    public Flux<Bid> findAll() {
        return bidRepository.findAll();
    }
}
