package com.example.tradie.services;

import com.example.tradie.dto.TradePeopleSetReqDto;
import com.example.tradie.models.TradesPeople;
import com.example.tradie.repositories.TradesPeopleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class TradesPersonService {

    private final TradesPeopleRepository tradesPeopleRepository;

    public Mono<TradesPeople> save(TradesPeople tradesPeople){
        return tradesPeopleRepository.save(tradesPeople);
    }

    public Flux<TradesPeople> findAllTradesPeople(){
        return tradesPeopleRepository.findAll();
    }

    public Mono<TradesPeople> findTradesPeople(String tradesPeopleId){
        return tradesPeopleRepository.findById(tradesPeopleId);
    }
    public Mono<TradesPeople> login(TradePeopleSetReqDto tradePeopleSetReqDto) {
        return tradesPeopleRepository.findByUsernameAndEmail(tradePeopleSetReqDto.getUsername(), tradePeopleSetReqDto.getEmail());
    }
}
