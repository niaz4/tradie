package com.example.tradie.services;

import com.example.tradie.models.Bid;
import com.example.tradie.models.Project;
import com.example.tradie.repositories.BidRepository;
import com.example.tradie.repositories.ProjectRepository;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProjectService {

    private final ProjectRepository projectRepository;

    public Mono<Project> saveProject(Project project){
        return projectRepository.save(project);
    }

    public Flux<Project> findAllProjects() {
        return projectRepository.findAllByOrderByCreatedAtDesc();
    }

    public Mono<Project> findProject(String projectId) {
        return projectRepository.findById(projectId);
    }
}
