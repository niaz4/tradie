package com.example.tradie.controllers;

import com.example.tradie.models.Project;
import com.example.tradie.services.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/projects")
@CrossOrigin("*") // This will not be here and we will control this using WAF and other infrastruce trick
@ControllerAdvice
public class ProjectController {

    private final ProjectService projectService;

    @GetMapping
    public Flux<Project> getAll(){
        return projectService.findAllProjects();
    }

    @GetMapping("/{projectId}")
    public Mono<Project> getProject(@PathVariable String projectId){
        return projectService.findProject(projectId);
    }

    @PostMapping
    public Mono<Project> create(@RequestBody Project project){
        return projectService.saveProject(project);
    }

    @GetMapping("/show-error")
    public Mono<Project> showError(){
        throw new RuntimeException("Error occurred");
    }
}
