package com.example.tradie.controllers;

import com.example.tradie.dto.TradePeopleSetReqDto;
import com.example.tradie.models.TradesPeople;
import com.example.tradie.services.TradesPersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/trades-people")
@RequiredArgsConstructor
@CrossOrigin("*") // This will not be here and we will control this using WAF and other infrastruce trick
@ControllerAdvice
public class TradesPeopleController {

    private final TradesPersonService tradesPersonService;

    @PostMapping
    public Mono<TradesPeople> save(@RequestBody TradesPeople tradesPeople){
        return tradesPersonService.save(tradesPeople);
    }

    @PostMapping("/set")
    public Mono<TradesPeople> login(@RequestBody TradePeopleSetReqDto tradePeopleSetReqDto){
        System.out.println("tradePeopleSetReqDto.toString() = " + tradePeopleSetReqDto.toString());
        return tradesPersonService.login(tradePeopleSetReqDto);
    }
}
