package com.example.tradie.controllers;

import com.example.tradie.models.Bid;
import com.example.tradie.services.BidService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("/bids")
@RequiredArgsConstructor
@CrossOrigin("*") // This will not be here and we will control this using WAF and other infrastruce trick
public class BidController {

    private final BidService bidService;

    @GetMapping
    public Flux<Bid> getAll(){
        return bidService.findAll();
    }

    @GetMapping("/{projectId}")
    public Flux<Bid> getAllBidByProjectId(@PathVariable String projectId){
        return bidService.findAllByProjectId(projectId);
    }

    @PostMapping
    public Mono<Bid> save(@Valid @RequestBody Bid bid){
        return bidService.save(bid);
    }
}
