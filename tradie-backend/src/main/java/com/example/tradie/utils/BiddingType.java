package com.example.tradie.utils;

public enum BiddingType {
    FIXED, HOURLY
}
