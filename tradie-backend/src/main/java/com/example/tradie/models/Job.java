package com.example.tradie.models;

import com.example.tradie.utils.BiddingType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Document(collection = "jobs")
@NoArgsConstructor
@AllArgsConstructor
public class Job {

    @Id
    private String id;

    @NotNull(message = "project id is required")
    private String projectId;

    @NotNull(message = "trade people id is required")
    private String tradesPersonId;

    @NotNull(message = "bid id is required")
    private String bidId;

    @NotNull(message = "Bid type is required")
    private BiddingType jobType;

    @NotNull(message = "amount is required")
    private Integer amount;

    private LocalDateTime createdAt = LocalDateTime.now();

}
