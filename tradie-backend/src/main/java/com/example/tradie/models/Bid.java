package com.example.tradie.models;

import com.example.tradie.utils.BiddingType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Document(collection = "bids")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Bid {

    @Id
    private String id;

    @NotNull(message = "trade people id is required")
    private String tradesPeopleId;

    @NotNull(message = "project id is required")
    private String projectId;

    @NotNull(message = "Bidding Type is required")
    private BiddingType biddingType;

    @NotNull(message = "Bid amount is required")
    private Integer amount;

    private LocalDateTime createdAt = LocalDateTime.now();
}
