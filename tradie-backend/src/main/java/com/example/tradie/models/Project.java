package com.example.tradie.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Document(collection = "projects")
public class Project {

    @Id
    private String id;

    @NotNull(message = "title is required")
    private String title;

    @NotNull(message = "description is required")
    private String description;

    @NotNull(message = "duration is required")
    private Integer duration;

    @NotNull(message = "deadline is required")
    private String deadline;

    @NotNull(message = "creator id is required")
    private String createdBy;

    private String jobId;

    private LocalDateTime createdAt = LocalDateTime.now();
}
