package com.example.tradie.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Data
@Document(collection = "tradespeople")
public class TradesPeople {

    @Id
    private String id;

    @NotNull(message = "username is required")
    private String username;

    @NotNull(message = "email is required")
    private String email;

}
