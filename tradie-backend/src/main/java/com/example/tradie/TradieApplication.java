package com.example.tradie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradieApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradieApplication.class, args);
	}

}
