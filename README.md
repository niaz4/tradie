
# Tradie


# Run Locally
You can start by doing the following steps:

**Make sure to install JDK 11. This project only runs on jdk 11.
Make sure to install node 14.**

## _Installation_
clone the project from gitlab
```bash
    git clone https://gitlab.com/niaz4/tradie.git
```

## _Database initilization_
database name: tradie


## _Run Backend_

*move to the root directory*
```bash
    cd tradie-backend
```

*~/tradie-backend install all the required package and build*
```bash
    mvn clean install -DskipTests
```

```bash
    mvn spring-boot:run
```

*port is listening at 8181*

## _Run Frontend_

*move to the root directory*
```bash
    cd tradie-frontend
```

*Install required dependencies*
```bash
    npm install
```

*running the frontend*
```bash
    npm start
```

*port is listening at 3000*