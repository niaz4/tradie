import { Subject } from "rxjs";
import { ITradePeople } from "../services/AuthService";

export const auth$ = new Subject<boolean>();
let initialAuth: boolean = false; 
auth$.subscribe(auth => {
    initialAuth = auth;
});

export const tradePeople$ = new Subject<ITradePeople>();
let initialTradePeople: ITradePeople = {
    id: "",
    username: "",
    email: ""
}; 
tradePeople$.subscribe(tradePeople => {
    initialTradePeople = tradePeople;
});

export { initialAuth, initialTradePeople };
