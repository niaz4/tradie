import { Subject } from "rxjs";
import { IBid } from "../services/BidService";


export const bids$ = new Subject<IBid[]>();

let initialBids: IBid[] = []; 
bids$.subscribe(bids => {
    initialBids = bids;
});


export { initialBids };