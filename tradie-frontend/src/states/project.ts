import { Subject } from "rxjs";
import { IProject } from "../services/ProjectService";

const projectDetail$ = new Subject<IProject>();
let initialProjectDetail: IProject = {
    id: "",
    title: "",
    description: "",
    duration: 0,
    deadline: "",
    createdBy: "",
    jobId: ""
};
projectDetail$.subscribe(sub => {
    initialProjectDetail = sub;
})


const projects$ = new Subject<IProject[]>();
let initialProjects: IProject[] = []; 
projects$.subscribe(sub => {
    initialProjects = sub;
});

export {
    initialProjectDetail,
    projectDetail$,
    initialProjects,
    projects$
};