import { CalendarOutlined, ClockCircleOutlined, DollarOutlined, HddOutlined } from '@ant-design/icons';
import { Avatar, Button, Col, Form, Input, InputNumber, List, message, Modal, Row, Select } from 'antd';
import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import useObservable from '../hooks/useObservable';
import { ITradePeople } from '../services/AuthService';
import { bidService, IBid } from '../services/BidService';
import { IProject, projectService } from '../services/ProjectService';
import { initialTradePeople, tradePeople$ } from '../states/auth';
import { bids$, initialBids } from '../states/bid';
import { initialProjectDetail, projectDetail$ } from '../states/project';

const BidPage = () => {

  const { id: projectId } = useParams();

  const bids: IBid[] = useObservable(bids$, initialBids);
  const projectDetail: IProject = useObservable(projectDetail$, initialProjectDetail);
  const tradePeople: ITradePeople = useObservable(tradePeople$, initialTradePeople);

  const [openBidForm, setOpenBidForm] = useState<boolean>(false);


  useEffect(()=>{
    if(projectId) {
      projectService.fetchProject(projectId);
      
    }
    const timer = setInterval(()=>{
      if(projectId) bidService.fetchByProjectId(projectId);
    },1800);

    return () => clearInterval(timer); 
  }, [projectId]);
  

  const onFinishBid = (values: IBid) => {
    console.log('Success:', values);
    bidService.createBid({...values})
    .then(()=> {
      message.success("Bid is successfully placed");
      setOpenBidForm(false);
    })
    .catch(()=> message.error("Bid can not be placed"));
  };

  const onFinishFailedBid = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };





  return (
    <div>
      {/* Project Detail */}
      <Row>
        <Col span={16} offset={4}>
          <List
            bordered
            style={{"background": "#fff", "boxShadow": "0 5px 5px #7771"}}
            header={<div style={{"fontSize": "20px", "fontWeight": "bold"}}>Project Details</div>}
            className="demo-loadmore-list"
            loading={[projectDetail] && [projectDetail]?.length <= 0}
            itemLayout="horizontal"
            // loadMore={loadMore}
            dataSource={[projectDetail]}
            renderItem={(item) => (
                <List.Item>
                    <List.Item.Meta
                      avatar={<Avatar size={"large"} shape="square" style={{"marginTop": "25px"}} src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" />}
                      title={<a href="https://ant.design">{item.title}</a>}
                      description={item.description}
                    />
                    <List.Item.Meta
                      avatar={<ClockCircleOutlined  style={{"marginTop": "25px"}} />}
                      title={<>{item.duration} hours</>}
                      description={"Total hours to finish the job"}
                    />
                    <List.Item.Meta
                      avatar={<CalendarOutlined  style={{"marginTop": "25px"}} />}
                      title={<>{item.deadline}</>}
                      description={"Last time to bid"}
                    />
                    <Button onClick={()=> setOpenBidForm(true)} >Bid Now</Button>
                </List.Item>
            )}
          />
        </Col>
      </Row>
      <br />

      {/* Bid List */}
      <Row>
        <Col span={16} offset={4}>
          <List
            bordered
            style={{"background": "#fff", "boxShadow": "0 5px 5px #7771"}}
            header={<div style={{"fontSize": "20px", "fontWeight": "bold"}}>Bids Until Now</div>}
            // className="demo-loadmore-list"
            // loading={bids && bids?.length <= 0}
            itemLayout="horizontal"
            // loadMore={loadMore}
            dataSource={bids}
            renderItem={(item: IBid) => (
                <List.Item>
                    <List.Item.Meta
                      avatar={<Avatar size={"large"} shape="square" style={{"marginTop": "25px"}} src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" />}
                      title={<a href="https://ant.design">{item.id}</a>}
                      description={item.tradesPeopleId}
                    />
                    <List.Item.Meta
                      avatar={<HddOutlined  style={{"marginTop": "25px"}} />}
                      title={<>TYPE: {item.biddingType}</>}
                      description={"Type of rate 'FIXED' | 'HOURLY'"}
                    />
                    <List.Item.Meta
                      avatar={<DollarOutlined  style={{"marginTop": "25px"}} />}
                      title={<>AMOUNT: ${item.amount}</>}
                      description={"Amount to be paid as per the type"}
                    />
                    {/* <Button>Bid Now</Button> */}
                </List.Item>
            )}
          />
        </Col>
      </Row>

      {/* Bid Place Form */}
      <Modal
        title={<div style={{"paddingRight": "30px"}}>Please confirm before you bid, once you place your bid you can't undo.</div>}
        centered
        open={openBidForm}
        onOk={() => setOpenBidForm(false)}
        onCancel={() => setOpenBidForm(false)}
      >
        <p>Project ID: {projectDetail.id}</p>
        <Form
            name={"add_bid"}
            onFinish={onFinishBid}
            onFinishFailed={onFinishFailedBid}
            layout="vertical"
            autoComplete="off"
            initialValues={{projectId: projectDetail.id, tradesPeopleId: tradePeople.id}}
        >
            <Form.Item
                name="projectId"
                hidden
            >
                <Input type={"hidden"} />
            </Form.Item>

            <Form.Item
                name="tradesPeopleId"
                hidden
            >
                <Input type={"hidden"} />
            </Form.Item>

            <Row>
                <Col span={8}>
                    <Form.Item
                        label="Amount"
                        name="amount"
                        rules={[{ required: true, message: 'Please input project description!' }]}
                    >
                        <InputNumber />
                    </Form.Item>
                </Col>

                <Col span={16}>
                    <Form.Item
                        label="Type"
                        name="biddingType"
                        rules={[{ required: true, message: 'Please input project description!' }]}
                    >
                        <Select
                          options={[
                            {
                              value: 'FIXED',
                              label: 'FIXED',
                            },
                            {
                              value: 'HOURLY',
                              label: 'HOURLY',
                            },
                          ]}
                        />
                    </Form.Item>
                </Col>
            </Row>


            <Form.Item>
                <Button block size="large" type="primary" htmlType="submit">
                    PLACE BID
                </Button>
            </Form.Item>
        </Form>
      </Modal>
    </div>
  )
}

export default BidPage;