import { Button, Card, Col, Form, Input, message, Row, Tabs } from 'antd';
import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import useObservable from '../hooks/useObservable';
import { authService, ITradePeople } from '../services/AuthService';
import { auth$, initialAuth } from '../states/auth';

const LoginPage = () => {

    const navigate = useNavigate();
    
    const auth = useObservable(auth$, initialAuth);
    
    const [selectedTabKey, setSelectedTabKey] = useState<string>("1");


    useEffect(()=>{
        if(auth){
            navigate("/")
        }
    },[auth, navigate])

    const onFinish = (values: ITradePeople) => {
        console.log('Success:', values);
        if(selectedTabKey === "1"){
            // login
            authService.login({...values}).then(data => {
                if(!data) message.error("login Unsuccessful")
            }).catch(() => message.error("login Unsuccessful"));
        }
        else if(selectedTabKey === "2"){
            // sign up
            authService.register({...values}).then(data => {
                if(!data) message.error("Registration Unsuccessful");
            }).catch(() => message.error("Registration Unsuccessful"));
        }
    };
    
    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <Row>
            <Col span={6} offset={9}>
            <Tabs
                defaultActiveKey={selectedTabKey}
                centered
                onChange={(activeKey: string)=> setSelectedTabKey(activeKey)}
                items={new Array(2).fill(null).map((_, i) => {
                const id = String(i + 1);
                return {
                    label: id === "1" ? "LOGIN" : "SIGN UP",
                    key: id,
                    children: <Card title={id === "1" ? "LOGIN" : "SIGN UP"} bordered={false}>
                                    <Form
                                        name={id === "1" ? "LOGIN" : "SIGN UP"}
                                        onFinish={onFinish}
                                        onFinishFailed={onFinishFailed}
                                        layout="vertical"
                                        autoComplete="off"
                                    >
                                        <Form.Item
                                            label="Username"
                                            name="username"
                                            rules={[{ required: true, message: 'Please input your username!' }]}
                                        >
                                            <Input />
                                        </Form.Item>
                    
                                        <Form.Item
                                            label="Email"
                                            name="email"
                                            rules={[{ required: true, message: 'Please input your email!' }]}
                                        >
                                            <Input />
                                        </Form.Item>
                    
                                        <Form.Item>
                                            <Button block size="large" type="primary" htmlType="submit">
                                                Submit
                                            </Button>
                                        </Form.Item>
                                    </Form>
                                </Card>,
                };
                })}
            />
            </Col>
        </Row>
    );
}

export default LoginPage;