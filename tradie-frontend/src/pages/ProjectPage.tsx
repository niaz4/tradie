import { PlusOutlined } from '@ant-design/icons';
import { Button, Col, DatePicker, Drawer, FloatButton, Form, Input, InputNumber, message, Row } from 'antd';
import { useEffect, useState } from 'react'
import Project from '../components/project/Project';
import useObservable from '../hooks/useObservable';
import { ITradePeople } from '../services/AuthService';
import { IProject, projectService } from '../services/ProjectService';
import { initialTradePeople, tradePeople$ } from '../states/auth';
import { initialProjects, projects$ } from '../states/project';


const ProjectPage = () => {

    const [openDrawer, SetOpenDrawer] = useState<boolean>(false);
    const [deadline, setDeadline] = useState<string>("");

    const projects: IProject[] = useObservable(projects$, initialProjects);
    const tradePeople: ITradePeople = useObservable(tradePeople$, initialTradePeople);


    useEffect(()=>{
        projectService.fetchProjects();
    }, [])

    const onFinish = (values: IProject) => {
        console.log('Success:', values);
        values.createdBy = tradePeople.id ? tradePeople.id : "";
        values.deadline = deadline;
        values.jobId = null;
        projectService.createProject({...values})
        .then(()=> {message.success("Project successfully created"); SetOpenDrawer(false)})
        .catch(()=> message.success("Project successfully created"))
        
    };
    
    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };


    

  return (
    <Row>
        {
            projects.map((project, i)=> (
                <Project key={i} data={project} />
            ))
        }
        <Drawer
            title="Add new project"
            placement="right"
            closable={false}
            onClose={()=>SetOpenDrawer(false)}
            open={openDrawer}
            key="right"
        >
            <Form
                name={"add_project"}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                layout="vertical"
                autoComplete="off"
            >
                <Form.Item
                    label="Title"
                    wrapperCol={{span: 24}}
                    name="title"
                    rules={[{ required: true, message: 'Please input project title!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Description"
                    name="description"
                    rules={[{ required: true, message: 'Please input project description!' }]}
                >
                    <Input.TextArea rows={4} />
                </Form.Item>

                <Row>
                    <Col span={8}>
                        <Form.Item
                            label="Duration"
                            name="duration"
                            rules={[{ required: true, message: 'Please input project description!' }]}
                        >
                            <InputNumber />
                        </Form.Item>
                    </Col>

                    <Col span={16}>
                        <Form.Item
                            label="Deadline"
                            name="deadline"
                            rules={[{ required: true, message: 'Please input project description!' }]}
                        >
                            <DatePicker format="YYYY-MM-DD HH:mm:ss" showTime onChange={(M: any, v: string) => setDeadline(v)} />
                        </Form.Item>
                    </Col>
                </Row>


                <Form.Item>
                    <Button block size="large" type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </Drawer>
        <FloatButton onClick={()=>SetOpenDrawer(true)} type="primary" icon={<PlusOutlined />} />
    </Row>
  )
}

export default ProjectPage;