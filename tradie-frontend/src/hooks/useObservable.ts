import { useEffect, useState } from "react";

const useObservable = (observable: any, initialState: any) => {
  const [state, setState] = useState(initialState);

  useEffect(() => {
    const sub = observable.subscribe(setState);
    return () => sub.unsubscribe();
  }, [observable]);

  return state;
};

export default useObservable;