import { Layout, Menu } from 'antd';
import { Content, Footer, Header } from 'antd/es/layout/layout';
import { Outlet, useNavigate } from 'react-router-dom';
import './App.css';


function App() {

  const navigate = useNavigate();

  const menuItems = [
    {
      key: "1",
      label: "HOME",
      link: "/"
    },
    {
      key: "2",
      label: "LOGIN",
      link: "/login"
    },
  ];

  const onChangePage = (key: string) => {
    const link = menuItems.find(m=>m.key===key)?.link;
    if(link) navigate(link);
  }

  return (
    <Layout>
      <Header>
        <div style={{float: "left",
          width: "120px",
          height: "31px",
          margin: "16px 24px 16px 0",
          background: "rgba(255, 255, 255, 0.3)"}} 
        />
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={['2']}
          onSelect={(v: any)=>onChangePage(v.key)}
          items={menuItems}
        />
      </Header>
      <Layout>
        <Content style={{height: "100vh", padding: "20px"}}><Outlet /></Content>
      </Layout>
      <Footer>footer</Footer>
    </Layout>
  );
}

export default App;
