import { CalendarOutlined, ClockCircleOutlined, EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons';
import { Avatar, Card } from 'antd';
import Meta from 'antd/es/card/Meta';
import { useNavigate } from 'react-router-dom';
import { IProject } from '../../services/ProjectService';

const Project = ({
  data
}: {
  data: IProject
}) => {

  const navigate = useNavigate();

  return (
      <Card
        style={{ width: 300, margin: "10px" }}
        cover={
          <img
            alt="example"
            src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
          />
        }
        actions={[
          <SettingOutlined onClick={()=>navigate(`/projects/${data.id}`)} key="setting" />,
          <EditOutlined onClick={()=>navigate(`/projects/${data.id}`)} key="edit" />,
          <EllipsisOutlined onClick={()=>navigate(`/projects/${data.id}`)} key="ellipsis" />,
        ]}
      >
        <Meta
          avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
          title={data.title}
          description={<>
                          <span>{data.description}</span>
                          <br />
                          <span><ClockCircleOutlined /> {data.duration} hrs</span>
                          <br />
                          <span><CalendarOutlined /> {data.deadline}</span>
                          
                        </>
                      }
        />
      </Card>
  )
}

export default Project;