import React from 'react'

const ProjectList = ({
    className,
    header,
    headerSubtitle,
    type,
    footer,
    footerTemplate,
    children
}: {
    className?: string;
    header?: string,
    headerSubtitle?: string,
    type?: "grid" | "list";
    footer?: string;
    footerTemplate: React.ReactNode;
    children: React.ReactNode
}) => {
  return (
    <div className={className}>
        
        { children }
    </div>
  )
}

export default ProjectList;