import { AxiosResponse } from "axios";
import { initialProjects, projectDetail$, projects$ } from "../states/project";
import { http } from "./http";

export interface IProject{
    id: string;

    title: string;

    description: string;

    duration: number;

    deadline: string;

    createdBy: string;

    jobId?: string | null;
}

class ProjectService {

    
    fetchProject(projectId: string){
        http.get(`/projects/${projectId}`).then((response: AxiosResponse<IProject, any>) => {
            projectDetail$.next(response.data);
        });
    }

    fetchProjects(){
        http.get("/projects").then((response: AxiosResponse<IProject[], any>) => {
            projects$.next(response.data);
        });
    }


    createProject(project: IProject){
        return http.post("/projects", {...project, id: null}).then((response: AxiosResponse<IProject, any>) => {
            projects$.next([response.data, ...initialProjects]);
            return response.data;
        });
    }
    
}

export const projectService = new ProjectService();