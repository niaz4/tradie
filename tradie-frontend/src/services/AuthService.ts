import { AxiosResponse } from "axios";
import { auth$, tradePeople$ } from "../states/auth";
import { http } from "./http";

export interface ITradePeople {
    id?: string;
    email: string;
    username: string;
}
  

class AuthService {

    
    register(tradePeople: ITradePeople): Promise<ITradePeople> {
        return http.post("/trades-people", {...tradePeople}).then((response: AxiosResponse<ITradePeople, any>) => {
            auth$.next(true);
            tradePeople$.next(response.data);
            return response.data;
        });
    }

    login(tradePeople: ITradePeople): Promise<ITradePeople>{
        return http.post("/trades-people/set", {...tradePeople}).then((response: AxiosResponse<ITradePeople, any>) => {
            if(response.data && response.data.id){
                auth$.next(true);
                tradePeople$.next(response.data);
            }
            return response.data;
        });
    }

}

export const authService = new AuthService();