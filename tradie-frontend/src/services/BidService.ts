import { AxiosResponse } from "axios";
import { bids$, initialBids } from "../states/bid";
import { http } from "./http";

export interface IBid {
    id?: string;
    tradesPeopleId: string;
    projectId: string;
    biddingType: string;
    amount: number;
}

  
class BidService {

    fetchByProjectId(projectId: string){
        return http.get(`/bids/${projectId}`).then((response: AxiosResponse<IBid[], any>) => {
            bids$.next(response.data);
            return response.data;
        });
    }

    createBid(bid: IBid){
        return http.post("/bids", {...bid}).then((response: AxiosResponse<IBid, any>) => {
            bids$.next([response.data, ...initialBids]);
            return response.data;
        });
    }

}

export const bidService = new BidService();